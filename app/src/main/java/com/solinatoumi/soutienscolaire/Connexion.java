package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Connexion extends AppCompatActivity {
    Button espaceParent;
    Button espaceEtudiant;

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion_activity);

        espaceParent = findViewById(R.id.Parent);
        espaceEtudiant =  findViewById(R.id.Etudiant);

        espaceEtudiant.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Log.i("TAG", "Onclick etudiant");
                Intent intent = new Intent(Connexion.this, Formulaire_login_Activity.class);
                intent.putExtra("role","etudiant");
                Log.i("info","role etudiant");
                startActivity(intent);

            }

        });

        espaceParent.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Log.i("TAG", "Onclick parent");
                Intent intent = new Intent(Connexion.this, Formulaire_login_Activity .class);
                intent.putExtra("role","parent");
                Log.i("info","role parent");

                startActivity(intent);

            }

        });
    }
}
