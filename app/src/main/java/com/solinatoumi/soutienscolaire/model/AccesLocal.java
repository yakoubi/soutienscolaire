package com.solinatoumi.soutienscolaire.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.solinatoumi.soutienscolaire.outil.MySQLiteOpenHelper;

public class AccesLocal {
 private String nomBase ="soutienScolaireDb.sqlite";
private Integer versionBase = 1;
private MySQLiteOpenHelper accesBd;
SQLiteDatabase db;

    public AccesLocal(Context context) {
        accesBd = new MySQLiteOpenHelper(context, nomBase, null,versionBase);
    }

    public void ajouterUser(User user){
        Log.i("info","ajouter user...");
        db = accesBd.getWritableDatabase();
        String req = "INSERT INTO USER (nom,prenom,email,pw,role,niveau)";

        req+= "VALUES("+
               "\""+ user.getNom()+"\","+
                "\""+ user.getPrenom()+"\","+
                "\""+ user.getEmail()+"\","+
                "\""+ user.getPw()+"\","+
                "\""+ user.getRole()+"\","+
                + user.getNiveau()+
                ")";
        db.execSQL(req);
        Log.i("info","fin ajouter user.");

    }

    public boolean verifierUser(String mail) {
        Log.i("info", "verifier l'existence d'un utilisateur: mail:" + mail );
        db = accesBd.getReadableDatabase();
        String req = "select * from user where email ='" + mail+"'";
        Cursor cursor = db.rawQuery(req, null);
        Log.i("info", "cursor: "+cursor.getCount());
        if (cursor!=null && cursor.getCount()!=0) {
            //cursor.getString(3);
            return true;
        } else {
            return false;
        }
    }

}
