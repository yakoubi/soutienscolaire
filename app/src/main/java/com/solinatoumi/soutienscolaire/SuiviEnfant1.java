package com.solinatoumi.soutienscolaire;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SuiviEnfant1 extends AppCompatActivity {

    Button ParcoursPedagogique;
    Button BilanDapprentissage;
    Button GererAbonnement;

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.suivi_enfant1);

        ParcoursPedagogique = (Button) findViewById(R.id.parcourspedagogique);
        BilanDapprentissage = (Button) findViewById(R.id.bilanapprentissage);
        GererAbonnement = (Button) findViewById(R.id.abonnement);


        ParcoursPedagogique.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(SuiviEnfant1.this, Parcourspedagogique.class);
                startActivity(intent);

            }
        });



        BilanDapprentissage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(SuiviEnfant1.this, BilanApprentissage.class);
                startActivity(intent);
            }
        });


        GererAbonnement.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(SuiviEnfant1.this, GestionAbonnement.class);
                startActivity(intent);
            }
        });
    }

    public Boolean ValidateUsername(String username) {
        boolean validate = true;

        Pattern P = Pattern.compile("(.*[a-zA-Z])");

        Matcher m = P.matcher(username);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }



        //Vérifier si la donnée existe déjà dans la base de données

        // if () //validate = false si les données existent déjà, puis changer la valeur du message à envoyer au client





        return validate;

    }

}
