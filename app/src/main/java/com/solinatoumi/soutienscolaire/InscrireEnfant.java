package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InscrireEnfant  extends AppCompatActivity {

    EditText nom,prenom,niveauscolaire, mail;
    Button valider;
    Spinner spinner1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.formualire_inscription_eleve);

        nom = (EditText) findViewById(R.id.nomeleve);
        prenom = (EditText) findViewById(R.id.prenomeleve);
        niveauscolaire = (EditText) findViewById(R.id.niveauscolaire);
        mail = (EditText) findViewById(R.id.mail);
        valider = (Button) findViewById(R.id.valider);



        mail.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                if(!hasFocus){
                    if (Validatemail(mail.getText().toString())){

                        Toast.makeText(getApplicationContext(),"Valide",Toast.LENGTH_SHORT).show();

                    }

                    else {

                        Toast.makeText(getApplicationContext(),"Format invalide, exemple : sarah@hotmail.fr ",Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });





        nom.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                if(!hasFocus){
                    if (ValidateUsername(nom.getText().toString())){

                        Toast.makeText(getApplicationContext(),"Nom Valide",Toast.LENGTH_SHORT).show();

                    }

                    else {

                        Toast.makeText(getApplicationContext(),"Uniquement des caractères de l'alphabet",Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });


        prenom.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                if(!hasFocus){
                    if (Validatemobile(prenom.getText().toString())){

                        Toast.makeText(getApplicationContext(),"Numéro Valide",Toast.LENGTH_SHORT).show();

                    }

                    else {

                        Toast.makeText(getApplicationContext(),"10 chiffres successifs !",Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });




        valider.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Toast.makeText(InscrireEnfant.this,
                        "OnClickListener : " +
                                "\nSpinner : "+ String.valueOf(spinner1.getSelectedItem()),
                        Toast.LENGTH_SHORT).show();

            }
        });




    }


    public Boolean ValidateUsername(String username) {
        boolean validate = true;

        Pattern P = Pattern.compile("(.*[a-zA-Z])");

        Matcher m = P.matcher(username);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }

        return validate;

    }


    public Boolean Validatemobile(String nombre){

        boolean validate = true;

        Pattern P = Pattern.compile("(\\d{10})");

        Matcher m = P.matcher(nombre);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }

        return validate;

    }







    public Boolean Validatemail(String mail){




        boolean validate = true;

        Pattern P = Pattern.compile("(^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$)", Pattern.CASE_INSENSITIVE);

        Matcher m = P.matcher(mail);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }

        return validate;

    }
}
