package com.solinatoumi.soutienscolaire;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ConnexionEtudiant extends AppCompatActivity {

    EditText  motDepass,username;
    Button seconnecter;


    @Override
    protected void onStart() {
        super.onStart();

        Log.i("TAG", "OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("TAG", "OnResume");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i("TAG", "OnCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulaire_connexion);


        seconnecter = (Button) findViewById(R.id.buttonId);
        username = (EditText) findViewById(R.id.username);
        motDepass = (EditText) findViewById(R.id.editTextPwId);



        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (ValidateUsername(username.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "Nom Valide", Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getApplicationContext(), "Nom doit être compris entre a et z", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        seconnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnexionEtudiant.this, MainActivity.class);
                startActivity(intent);

            }
        });




        seconnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnexionEtudiant.this,EspaceEtudiant.class);
                startActivity(intent);
            }
        });
    }

    public Boolean ValidateUsername(String username) {
        boolean validate = true;

        Pattern P = Pattern.compile("(.*[a-zA-Z])");

        Matcher m = P.matcher(username);
        boolean b = m.matches();


        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }


        //Vérifier si la donnée existe déjà dans la base de données

        // if () //validate = false si les données existent déjà, puis changer la valeur du message à envoyer au client


        return validate;

    }
}

