package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class BilanApprentissage extends AppCompatActivity {

    Button Bilan;

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("TAG", "OnStart");

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("TAG", "OnCreate");

        Log.i("TAG", "OnResume");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.bilan_apprentissage);


        Bilan = findViewById(R.id.bilanDamien);


        Bilan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BilanApprentissage.this, EspaceEtudiant.class);
                startActivity(intent);

            }

        });

    }


}

