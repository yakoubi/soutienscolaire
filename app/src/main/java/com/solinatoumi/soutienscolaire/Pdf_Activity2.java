package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class Pdf_Activity2 extends AppCompatActivity {
    GridView gridView;

    String[] fruitNames2 = {"java","php","angular","CSS","CSS","CSS","CSS"};
    int[] fruitImages2 = {R.drawable.pdf,R.drawable.pdf,R.drawable.pdf,R.drawable.pdf,R.drawable.pdf,R.drawable.pdf};
    String[] fileName = {"cours1.pdf","coursC2.pdf","coursPhp3.pdf","coursAngular4.pdf","coursCss5.pdf","coursCss5.pdf","coursCss5.pdf"};
    int[] videoImages = {R.drawable.video2,R.drawable.video2,R.drawable.video2,R.drawable.video2,R.drawable.video2,R.drawable.video2};
    String[] videoNames = {"java","php","angular","video4","video5","video6","video7"};

    String[] uri = {"uri1","uri1","uri1","uri1","uri1","uri1","uri1"};
    String recevedType ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_pdf_);
        Intent intent = getIntent();
        recevedType =   intent.getStringExtra("type");
        Log.i("info","recevedType**************:"+recevedType);
        //finding listview
        gridView = findViewById(R.id.gridview2);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                if(recevedType.equals("pdf")) {
                    Intent intent = new Intent(getApplicationContext(), GridItemActivity2.class);
                    intent.putExtra("name", fruitNames2[i]);
                    intent.putExtra("image", fruitImages2[i]);
                    intent.putExtra("fileName", fileName[i]);

                    intent.putExtra("type", recevedType);
                    intent.putExtra("uri", uri[i]);

                    Log.i("info", "" + "name:" + fruitNames2[i] + "image:" + fruitImages2[i] + "fileName:" + fileName[i] + "*******");
                    startActivity(intent);
                }else if(recevedType.equals("video")){

                    Intent intent = new Intent(getApplicationContext(), video_Activity.class);
                    intent.putExtra("name", fruitNames2[i]);
                    intent.putExtra("image", fruitImages2[i]);
                    intent.putExtra("fileName", fileName[i]);

                    intent.putExtra("type", recevedType);
                    intent.putExtra("uri", uri[i]);

                    Log.i("info", "" + "name:" + uri[i] + "image:" + fruitImages2[i] + "*******");
                    startActivity(intent);
                }

            }
        });
    }




    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages2.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_pdf_data2,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits2);

            ImageView image = view1.findViewById(R.id.images2);

            if(recevedType.equals("pdf")){
                name.setText(fruitNames2[i]);
                image.setImageResource(fruitImages2[i]);

            }else{
                name.setText(videoNames[i]);
                image.setImageResource(videoImages[i]);

            }

            return view1;



        }





    }

}
