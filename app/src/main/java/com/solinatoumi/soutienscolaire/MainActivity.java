package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button Connexion;
    Button Inscription;
    Button Invite;

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("TAG", "OnStart");

    }

    @Override
    protected void onStop() {super.onStop(); }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("TAG", "OnCreate1");

        Log.i("TAG", "OnResume");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    Connexion = findViewById(R.id.connexion);
    Inscription = findViewById(R.id.inscription);
    Invite= findViewById(R.id.invite);

    Connexion.setOnClickListener(new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            Log.i("TAG", "Onclick ici");
            Intent intent = new Intent(MainActivity.this, Connexion.class);
            startActivity(intent);

        }

    });
        Inscription.setOnClickListener(new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(MainActivity.this, Inscription.class);

            startActivity(intent);
        }
    });

        Invite.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           Intent intent = new Intent(MainActivity.this, UtilisateursAnonyme.class);

            startActivity(intent);
        }

    });



}
}
