package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EspaceEtudiant1_Activity extends AppCompatActivity {
    GridView gridView;

    String[] fruitNames = {"COURS","VIDEO","EXCERCICE","TEST","BILAN","AUTRE"};
    int[] fruitImages = {R.drawable.pdf,R.drawable.video2,R.drawable.exercice,R.drawable.qcm,R.drawable.bilan,R.drawable.banana};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espace_etudiant1_);
        //finding listview
        gridView = findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                ///Intent intent = new Intent(getApplicationContext(),GridItemActivity.class);///???

                switch (i){
                    //si on clique sur une image pdf on fait une redirection vers l 'activité qui gere les pfds
                case 0:
                    Intent intent0 = new Intent(getApplicationContext(),Pdf_Activity2.class);
                    intent0.putExtra("name",fruitNames[i]);
                    intent0.putExtra("image",fruitImages[i]);
                    intent0.putExtra("type","pdf");
                      startActivity(intent0);
                    break;
                    //si on clique sur la deuxieme image c t a d video on fait uen redrection vers la vu qui gere les video
                    case 1:
                        Intent intent1 = new Intent(getApplicationContext(),Pdf_Activity2.class);
                        intent1.putExtra("name",fruitNames[i]);
                        intent1.putExtra("image",fruitImages[i]);
                        intent1.putExtra("type","video");

                        startActivity(intent1);
                        break;
                        //sion on affiche un message en attendant le fin des dev
                        default:
                            Toast.makeText(getApplicationContext(),fruitNames[i]+ "en cours de develeppement",Toast.LENGTH_LONG).show();


                }




            }
        });
    }




    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_pdf_data,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);
            ImageView image = view1.findViewById(R.id.imagesGrille);

            name.setText(fruitNames[i]);
            image.setImageResource(fruitImages[i]);
            return view1;



        }
    }

}
