package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.github.barteksc.pdfviewer.*;
import com.github.barteksc.pdfviewer.util.FitPolicy;
//import com.github.barteksc.pdfviewer.util.FitPolicy;

public class Espace_etudiant_Activity extends AppCompatActivity {
    private Button buttonCours1Pdf;
 private Button buttonVideo;
 private Button buttonGridPdf;
    private Button buttonGridPdf2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espace_etudiant_);
        init();

    }

private void init(){
    buttonCours1Pdf = (Button)findViewById(R.id.buttonCours1Pdf);
   // buttonVideo = (Button) findViewById(R.id.buttonVideo1Id);
    buttonGridPdf = (Button) findViewById(R.id.buttonGridPdf);
    buttonGridPdf2 = (Button) findViewById(R.id.buttonGridPdf2);

     downlod();
  //  playVideo();
    listerPdfCours();
    listerPdfCours2();

}

 private void downlod(){
     buttonCours1Pdf.setOnClickListener(new OnClickListener(){
         public void onClick(View v){
//PDF View

             PDFView pdfView = findViewById(R.id.pdfView1);

             pdfView.fromAsset("coursC2.pdf")
                     .enableSwipe(true) // allows to block changing pages using swipe
                     .swipeHorizontal(false)
                     .enableDoubletap(true)
                     .defaultPage(0)
                     .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                     .password(null)
                     .scrollHandle(null)
                     .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                     // spacing between pages in dp. To define spacing color, set view background
                     .spacing(0)
                     .pageFitPolicy(FitPolicy.WIDTH)
                     .load();

         }
     });
 }


 private void playVideo(){
     Log.i("info;","play video*****");
     //   VideoView videoView1= (VideoView) findViewById(R.id.videoView);
       // MediaController mediaController = new MediaController(this);
       // mediaController.setAnchorView(videoView1);
        //Uri uri = Uri.parse(""+
         //     "https://r6---sn-4g5edn7y.googlevideo.com/videoplayback?expire=1579230600&ei=KNEgXtLzL9OO1gKu2IXgCA&ip=188.237.148.12&id=o-AAht-IwxRO_M_eXNvXqaJqO0PtxkdMkn1kec80NlVSlq&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ratebypass=yes&dur=1046.894&lmt=1539524358386784&fvip=11&fexp=23842630&c=WEB&txp=5431432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cratebypass%2Cdur%2Clmt&sig=ALgxI2wwRAIgL5WMXe0sjIf0XOKJoPdgdhMaN2ZBYQnYs-AzmG7lNC4CIGtNY0LR3ML8lO1tYmypMfFHys1IMUL-mo0ne8wRlqsr&rm=sn-hvaquxaxjvh-3p8s76,sn-3c2ee7d&req_id=73ffd26a803da3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mip=2a01:e35:2ffe:2b90:711c:7656:8f9b:94c&mm=29&mn=sn-4g5edn7y&ms=rdu&mt=1579208993&mv=m&mvi=5&pl=36&lsparams=ipbypass,mip,mm,mn,ms,mv,mvi,pl&lsig=AHylml4wRAIgdzIf-X5w4uMhG92QEIBvzN5G51WrCsX-d2FiU9CO9TACIH6t4kohDjAJZm8f2ab8HtWsiiLuFPRta-QQI2a1U9O8");        videoView1.setMediaController(mediaController);
       // videoView1.setVideoURI(uri);
     //videoView1.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" +R.raw.cours2));
Log.i("info","ici2");
     //videoView1.requestFocus();
      //  videoView1.start();

 }

 private void listerPdfCours(){
     Log.i("info","button1 lister cours pdf image clické....");
     buttonGridPdf.setOnClickListener(new OnClickListener()
     {

         @Override
         public void onClick(View v)
         {

             Log.i("TAG", "Onclick buttonGridPdf");
             Intent intent = new Intent(Espace_etudiant_Activity.this, EspaceEtudiant1_Activity.class);
             startActivity(intent);

         }

     });
 }


    private void listerPdfCours2(){
        Log.i("info","button2 lister cours pdf clické....");
        buttonGridPdf2.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                Log.i("TAG", "Onclick buttonGridPdf2");
                Intent intent = new Intent(Espace_etudiant_Activity.this, Pdf_Activity2.class);
                startActivity(intent);

            }

        });
    }
}
