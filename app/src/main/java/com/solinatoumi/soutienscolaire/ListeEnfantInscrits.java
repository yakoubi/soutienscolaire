package com.solinatoumi.soutienscolaire;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ListeEnfantInscrits extends AppCompatActivity {
    Button Enfant1;
    Button Enfant2;

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_des_enfatns_inscrits);

        Enfant1 = (Button) findViewById(R.id.Enfant1);
        Enfant2 = (Button) findViewById(R.id.Enfant2);



        Enfant1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ListeEnfantInscrits.this, SuiviEnfant1.class);
                startActivity(intent);

            }
        });



        Enfant2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ListeEnfantInscrits.this, SuiviEnfant2.class);
                startActivity(intent);
            }
        });
    }

    public Boolean ValidateUsername(String username) {
        boolean validate = true;

        Pattern P = Pattern.compile("(.*[a-zA-Z])");

        Matcher m = P.matcher(username);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }



        //Vérifier si la donnée existe déjà dans la base de données

        // if () //validate = false si les données existent déjà, puis changer la valeur du message à envoyer au client





        return validate;

    }

}
