package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class NiveauScolaire extends AppCompatActivity {


    Button Primaire ;
    Button  College;
    Button Lycee;


    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.niveau_scolaire);

        Primaire = (Button) findViewById(R.id.primaire);
        College = (Button) findViewById(R.id. college);
        Lycee = (Button) findViewById(R.id.lycee);


        Primaire.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(NiveauScolaire.this, MainActivity.class);
                startActivity(intent);

            }
        });



        College .setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(NiveauScolaire.this, MainActivity.class);
                startActivity(intent);
            }
        });


        Lycee.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(NiveauScolaire.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


}
