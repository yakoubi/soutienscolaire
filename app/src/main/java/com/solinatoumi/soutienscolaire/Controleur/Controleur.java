package com.solinatoumi.soutienscolaire.Controleur;

import android.content.Context;

import com.solinatoumi.soutienscolaire.model.AccesLocal;
import com.solinatoumi.soutienscolaire.model.User;

public class Controleur {
    private static Controleur instance =null;
private static AccesLocal accesLocal;

    private Controleur(){}

    public static Controleur getInstance(Context context) {
        if(Controleur.instance == null){
            Controleur.instance = new Controleur();
            accesLocal = new AccesLocal(context);
        }
        return Controleur.instance;
    }

    //pour ajouter un nouveau utilisateur
    public void ajouterUser(User user){
accesLocal.ajouterUser(user);
    }

    // verifier l'existance d'un utilisateur avant de l'ajouter
    public boolean verifierUser(String email){
        return  accesLocal.verifierUser(email);
    }

}
